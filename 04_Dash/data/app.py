import logging
import dash
# ===== START LOGGER =====
logger = logging.getLogger(__name__)
root_logger = logging.getLogger()
root_logger.setLevel(logging.INFO)
sh = logging.StreamHandler()
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
sh.setFormatter(formatter)
root_logger.addHandler(sh)
import dash_core_components
import dash_html_components
import pandas as pd
import plotly.express as px
from sklearn.manifold import TSNE

desired_width = 320
pd.set_option("display.max_columns", 20)
pd.set_option("display.width", desired_width)



app = dash.Dash(__name__,)
def main():

    bigram_df = pd.read_csv("bigrams.csv", index_col=0)
    fig = px.bar(
        bigram_df[:20],
        x="ngram",
        y="count",
        title="Counts of top bigrams",
        template="plotly_white",
        labels={"ngram": "Bigram", "count": "Count"},
    )
    fig.update_layout(width=1200, height=500)
    fig.show()

if __name__ == '__main__':
    app.run_server(debug=True)