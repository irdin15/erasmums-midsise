

#Remove extra spaces
def compactSpaces(s):
    os = ""
    for c in s:
        if c!= " " or os[-1] != " ":
            os+= c
    return os


"""REMOVE ULRS"""


def remove_links(row):
    row_no_link = re.sub(r"https\S+","",row)
    return row_no_link


def get_translated_text_deepl(row):
    try: 
        API_KEY = '9775f609-f2fb-0305-e88a-f9925770e9b1'
        url = 'https://deepl.com/v2/translate'
        headers = { 
            'Host' : 'api.deepl.com',
            'User-Agent' : 'MyPythonScript',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept' : '*/*'

                }
        data = { 
            'text': row,
            'auth_key': API_KEY,
             'target_lang': 'de'
        }
        r = requests.post(url=url,data=data, headers=headers)
        print(r)
        r= r.text
        
        time.sleep(0.5)
        return r
    except Exception as e:
        raise e


def translate_rows (df):

    de_list = df.loc[df['Description'].apply(lambda x: detect(x) == 'en')]
    de_list.to_sql('en_jobs',engine,if exists = 'append',index=False)


def prepare_text_for_wordcloud_with_freq(df):

    def basic_clean(text):
  """
  A simple function to clean up the data. All the words that
  are not designated as a stop word is then lemmatized after
  encoding and basic regex parsing are performed.
  """
    wnl = nltk.stem.WordNetLemmatizer()
    stopwords = nltk.corpus.stopwords.words('english') + ADDITIONAL_STOPWORDS
    text = (unicodedata.normalize('NFKD', text)
        .encode('ascii', 'ignore')
        .decode('utf-8', 'ignore')
        .lower())
    words = re.sub(r'[^\w\s]', '', text).split()
    return [wnl.lemmatize(word) for word in words if word not in stopwords]

    tokn_text  = basic_clean(''.join(str(df.tolist())))
    top_N = 100

    word_dist = nltk.FreqDist([' '.join(x) for x in bigram])
    rslt = pd.DataFrame(word_dist.most_common(top_N),
                columns=['Word', 'Frequency'])
    